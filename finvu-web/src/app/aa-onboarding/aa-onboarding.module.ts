import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AaOnboardingRoutingModule } from './aa-onboarding-routing.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { DiscoverComponent } from './discover/discover.component';
import { LinkingResultComponent } from './linking-result/linking-result.component';
import { LinkComponent } from './link/link.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { SelectBankComponent } from './select-bank/select-bank.component';
import { MobileVerificationComponent } from './mobile-verification/mobile-verification.component';
import { ConsentRequestResultComponent } from './consent-request-result/consent-request-result.component';
import { MobileOtpComponent } from './mobile-otp/mobile-otp.component';
import { WebviewLoginComponent } from './webview-login/webview-login.component';
import { LinkedAccountMessageComponent } from './linked-account-message/linked-account-message.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { FiuConsentRequestComponent } from './fiu-consent-request/fiu-consent-request.component';
import { WebviewRegisterComponent } from './webview-register/webview-register.component';
import { EmptyNotificationListComponent } from './empty-notification-list/empty-notification-list.component';
import { ConsentConfirmationComponent } from './consent-confirmation/consent-confirmation.component';
import { DecryptConsentRequestComponent } from './decrypt-consent-request/decrypt-consent-request.component';
import { ConsentService } from 'src/app/services/consent-service';
import { UserOfflineMessageService } from 'src/app/services/user-offline-message-service';
import { DeclineConsentRequestComponent } from './decline-consent-request/decline-consent-request.component';
import { DiscoverAccountMessageComponent } from './discover-account-message/discover-account-message.component';
import { DeclineConsentConfimationComponent } from './decline-consent-confimation/decline-consent-confimation.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ForgotPasswordVerificationComponent } from './forgot-password-verification/forgot-password-verification.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { WebviewOtpRequestComponent } from './webview-otp-request/webview-otp-request.component';
import { OnboardingErrorPageComponent } from './onboarding-error-page/onboarding-error-page.component';
import { SimpleService } from 'src/app/services/simple-service';
import { WINDOW_PROVIDERS } from 'src/app/services/window.providers';
import { PostMesaageService } from 'src/app/services/post-mesaage-service';
import { HttpModule } from '@angular/http';
import { StayAliveRequestService } from 'src/app/services/stay-alive-service';
import { SelectModule } from 'ng-select';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule, MatCheckboxModule, MatFormFieldModule, MatDividerModule, 
         MatListModule, MatIconModule, MatInputModule, MatAutocompleteModule, 
         MatGridListModule } from '@angular/material';

@NgModule({
  imports: [
    FormsModule,
    NgbModule,
    CommonModule,
    AaOnboardingRoutingModule,
    ReactiveFormsModule,
    ChartsModule,
    SelectModule,
    MatCardModule,
    MatFormFieldModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    HttpModule,
    SelectModule,
    MatAutocompleteModule,
    BrowserModule,
    MatGridListModule,
    MatCheckboxModule
  ],
  providers: [ConsentService, UserOfflineMessageService, SimpleService, WINDOW_PROVIDERS, 
              PostMesaageService, StayAliveRequestService],
  declarations: [LoginComponent, RegisterComponent, DiscoverComponent, LinkingResultComponent, LinkComponent, SelectBankComponent, 
                 MobileVerificationComponent, ConsentRequestResultComponent, MobileOtpComponent, WebviewLoginComponent, LinkedAccountMessageComponent, 
                 NotificationsComponent, FiuConsentRequestComponent, WebviewRegisterComponent, EmptyNotificationListComponent, 
                 ConsentConfirmationComponent, DecryptConsentRequestComponent, DeclineConsentRequestComponent, DiscoverAccountMessageComponent, 
                 DeclineConsentConfimationComponent, ForgotPasswordComponent, ForgotPasswordVerificationComponent, 
                 OnboardingComponent, WebviewOtpRequestComponent, OnboardingErrorPageComponent]
})
export class AaOnboardingModule { }
