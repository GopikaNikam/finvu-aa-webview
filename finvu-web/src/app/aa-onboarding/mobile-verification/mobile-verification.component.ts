import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Observable, Subscription, timer } from 'rxjs';
import { Message } from 'src/app/services/message';
import { MT } from 'src/app/services/message-types';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { MobileVerificationVerifyRequest } from 'src/app/services/payloads/mobile-verification-verify'
import { MobileVerificationRequest } from 'src/app/services/payloads/mobile-verification';
import { LoginRequest } from 'src/app/services/payloads/login';
import { take, map } from 'rxjs/operators';
import { RESPONSE_STATUS } from 'src/app/services/util';

@Component({
  selector: 'app-mobile-verification',
  templateUrl: './mobile-verification.component.html',
  styleUrls: ['./mobile-verification.component.css',
  '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class MobileVerificationComponent implements OnInit {

  public subscription: Subscription; mobileVerificationVerifySubscription: Subscription;
  mobileVerificationSubscription: Subscription; subscriptionTimer: Subscription;
  public loginResponse: Observable<Message>; mobileVerificationVerifyResponse: Observable<Message>;
  mobileVerificationResponse: Observable<Message>;
  public mobileVerificationForm: FormGroup;
  public mobileNumber: String; userId: String;
  public mobile: any;
  public submitted = false; showlink = true;
  public counter$: Observable<number>; counterTime$: Observable<number>;
  public count = 15;

  constructor(private router: Router, private aaWebSocket: AaWebsocketService, formBuilder: FormBuilder) {

    this.mobileVerificationForm = formBuilder.group({
      'otp': ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{6,6}$'), this.otpLengthValidator])]
    });
  }

  ngOnInit() {

    this.loginResponse = this.aaWebSocket.on<Message>(MT.RES.LOGIN);
    this.mobileVerificationResponse = this.aaWebSocket.on<Message>(MT.RES.MOBILEVERIFICATION);
    this.mobileVerificationVerifyResponse = this.aaWebSocket.on<Message>(MT.RES.MOBILEVERIFICATIONVERIFY);

    var number = sessionStorage.getItem("mobileNo");

    if (number == null) {
      this.mobile = sessionStorage.getItem("mobileNumber");
      this.mobileNumber = this.mobile.substring(6, 10);
      console.log(this.mobile)
    } else {
      this.mobile = number
      this.mobileNumber = this.mobile.substring(6, 10);
      this.onLogin();
    }

    this.setTimer();
  }

  public setTimer() {
    this.count = 15;
    this.counterTime$ = timer(0, 1000).pipe(
      take(this.count),
      map(() => --this.count)
    );

    this.showlink = true;
    this.counter$ = timer(15000);
    this.subscriptionTimer = this.counter$.subscribe(() => {
      this.showlink = false;
    });
  }

  onMobileVerify() {

    if (sessionStorage.getItem("sid") != null) {
      const mobileVerificationVerifyPayload: MobileVerificationVerifyRequest = {
        mobileNum: this.mobile,
        otp: this.mobileVerificationForm.get('otp').value
      };

      if (mobileVerificationVerifyPayload.mobileNum == "" || mobileVerificationVerifyPayload.mobileNum == null
        || mobileVerificationVerifyPayload.otp == "" || mobileVerificationVerifyPayload.otp == null) {
        return;
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.MOBILEVERIFICATIONVERIFY);

      const mobileVerificationVerifyMessage: Message = {
        header: msgHeader,
        payload: mobileVerificationVerifyPayload
      };

      console.log(mobileVerificationVerifyMessage);
      this.aaWebSocket.send(mobileVerificationVerifyMessage);

      this.mobileVerificationVerifySubscription = this.mobileVerificationVerifyResponse.subscribe(value => {
        console.log(value);
        sessionStorage.setItem("sid", value.header.sid);
        this.mobileVerificationVerifySubscription.unsubscribe();

        var reponseStatus = value.payload["status"];
        if (reponseStatus == RESPONSE_STATUS.ACCEPT) {
          this.mobileVerificationForm.reset();
          this.router.navigate(['onboarding/linked-account-message']);
        } else if (reponseStatus == RESPONSE_STATUS.REJECT) {
          this.mobileVerificationForm.reset();
          console.log(value.payload["message"])
        } else {
          alert("Something went wrong.")
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  onMobileVerification() {
    this.setTimer();

    if (sessionStorage.getItem("sid") != null) {
      this.mobileNumber = this.mobile.substring(6, 10);

      const mobileVerificationRequestPayload: MobileVerificationRequest = {
        mobileNum: this.mobile
      };

      if (mobileVerificationRequestPayload.mobileNum == "" || mobileVerificationRequestPayload.mobileNum == null) {
        return;
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.MOBILEVERIFICATION);

      const mobileVerificationRequestMessage: Message = {
        header: msgHeader,
        payload: mobileVerificationRequestPayload
      };

      console.log(mobileVerificationRequestMessage);
      this.aaWebSocket.send(mobileVerificationRequestMessage);
      this.mobileVerificationSubscription = this.mobileVerificationResponse.subscribe(value => {
        console.log(value);
        this.mobileVerificationSubscription.unsubscribe();
        var reponseStatus = value.payload["status"];
        console.log("reponseStatus: ", reponseStatus);
        if (reponseStatus == RESPONSE_STATUS.SEND) {
          console.log(value.payload["message"])
        } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
          console.log(value.payload["message"])
        } else if (reponseStatus == RESPONSE_STATUS.REJECT) {
          console.log(value.payload["message"])
        } else {
          alert("Something went wrong.")
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  onLogin() {

    sessionStorage.removeItem("sid");
    const loginRequestPayload: LoginRequest = {
      username: sessionStorage.getItem("username"),
      password: sessionStorage.getItem("password")
    };

    if (loginRequestPayload.password == "" || loginRequestPayload.username == ""
      || loginRequestPayload.password == null || loginRequestPayload.username == null) {
      return;
    }

    const msgHeader = HeaderBuilder.build(null, MT.REQ.LOGIN);

    const loginRequestMessage: Message = {
      header: msgHeader,
      payload: loginRequestPayload
    };

    console.log(loginRequestMessage);
    this.aaWebSocket.send(loginRequestMessage);

    this.subscription = this.loginResponse.subscribe(value => {
      console.log(value);
      sessionStorage.setItem("sid", value.header.sid);
      this.subscription.unsubscribe();

      var reponseStatus = value.payload["status"];

      if (reponseStatus == RESPONSE_STATUS.SUCCESS) {
        this.onMobileVerification();
      } else if(reponseStatus == RESPONSE_STATUS.VERIFY) {
        this.onMobileVerification();
        //this.router.navigate(['onboarding/mobile-otp']);
      } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
        this.onLogin();
      } else if (reponseStatus == RESPONSE_STATUS.FAILURE) {
        alert(value.payload["message"])
      } else {
        alert("Something went wrong.")
      }
    });
  }

  keyWithNumber(event: any) {
    const pattern =/[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  public otpLengthValidator(control: FormControl) {
    if ((control.value) != null && (control.value).length > 6)
      return { 'validLength': true };
  }
}


