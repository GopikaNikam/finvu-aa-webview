import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedAccountMessageComponent } from './linked-account-message.component';

describe('LinkedAccountMessageComponent', () => {
  let component: LinkedAccountMessageComponent;
  let fixture: ComponentFixture<LinkedAccountMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedAccountMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedAccountMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
