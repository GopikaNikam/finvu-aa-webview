import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Message } from 'src/app/services/message';
import { Observable, Subscription } from 'rxjs';
import { MT } from 'src/app/services/message-types';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { ForgotPasswordRequest } from 'src/app/services/payloads/forgot-password';
import { ConfigService } from 'src/app/services/config/config-service';
import { RESPONSE_STATUS } from 'src/app/services/util';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class ForgotPasswordComponent implements OnInit {

  public forgotPasswordForm: FormGroup;
  public mobileNumber: String; mobile: String;
  public forgotPasswordResponse: Observable<Message>;
  public subscription: Subscription; forgotPasswordSubscription: Subscription;
  public username: any; handleId: String;

  constructor(private router: Router, private aaWebSocket: AaWebsocketService, formBuilder: FormBuilder, public configService: ConfigService) {

    this.forgotPasswordForm = formBuilder.group({
      userId: [null, Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._-]{6,30}$')])],
      mobile: ['', Validators.compose([Validators.required, Validators.pattern('[6-9]\\d{9}')])]
    });
    this.handleId = this.configService.userPostfixName;
  }

  ngOnInit() {
    this.forgotPasswordResponse = this.aaWebSocket.on<Message>(MT.RES.FORGOTPASSWORD);
  }

  onForgotPassword() {

    sessionStorage.removeItem("sid");

    if (this.forgotPasswordForm.get('userId').value.includes('@finvu.in')) {
      alert("username is not valid");

    } else {
      this.username = this.forgotPasswordForm.get('userId').value.includes('@finvu')
        ? this.forgotPasswordForm.get('userId').value
        : this.forgotPasswordForm.get('userId').value + this.handleId;

      const forgotPasswordRequestPayload: ForgotPasswordRequest = {
        userId: this.username,
        mobileNum: this.forgotPasswordForm.get('mobile').value
      };

      if (forgotPasswordRequestPayload.mobileNum == "" || forgotPasswordRequestPayload.mobileNum == null
        || forgotPasswordRequestPayload.userId == "" || forgotPasswordRequestPayload.userId == null) {
        return;
      }

      sessionStorage.setItem("forgotMobileNumber", this.forgotPasswordForm.get('mobile').value)
      sessionStorage.setItem("forgotUserId", this.username)

      const msgHeader = HeaderBuilder.build(null, MT.REQ.FORGOTPASSWORD);

      const forgotPasswordRequestMessage: Message = {
        header: msgHeader,
        payload: forgotPasswordRequestPayload
      };

      console.log("forgot password Request:", forgotPasswordRequestMessage);

      this.aaWebSocket.send(forgotPasswordRequestMessage);
      this.forgotPasswordSubscription = this.forgotPasswordResponse.subscribe(value => {
        console.log("forgot password Reponse:", value);
        this.forgotPasswordSubscription.unsubscribe();
        var reponseStatus = value.payload["status"];
        console.log("reponseStatus: ", reponseStatus);
        if (reponseStatus == RESPONSE_STATUS.SEND) {
          this.router.navigate(['onboarding/forgot-password-verification']);
          console.log(value.payload["message"])
        } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
          alert(value.payload["message"])
        } else if (reponseStatus == RESPONSE_STATUS.REJECT) {
          alert(value.payload["message"])
        } else {
          alert("Something went wrong.")
        }
      });
    }
  }

  onCancel() {
    this.router.navigate(['onboarding/webview-login']);
  }
}


