import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsentConfirmationComponent } from './consent-confirmation.component';

describe('ConsentConfirmationComponent', () => {
  let component: ConsentConfirmationComponent;
  let fixture: ComponentFixture<ConsentConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsentConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
