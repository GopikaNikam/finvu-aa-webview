import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebviewLoginComponent } from './webview-login.component';

describe('WebviewLoginComponent', () => {
  let component: WebviewLoginComponent;
  let fixture: ComponentFixture<WebviewLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebviewLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebviewLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
