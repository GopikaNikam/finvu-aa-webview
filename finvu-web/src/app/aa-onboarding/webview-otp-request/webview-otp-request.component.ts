import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Message } from 'src/app/services/message';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { MT } from 'src/app/services/message-types';
import { LoginRequest } from 'src/app/services/payloads/login';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { Router } from '@angular/router';
import { WebviewEncryptRequest } from 'src/app/services/payloads/webview-encrypt-request';
import { MobileVerificationVerifyRequest } from 'src/app/services/payloads/mobile-verification-verify';
import { RESPONSE_STATUS } from 'src/app/services/util';

@Component({
  selector: 'app-webview-otp-request',
  templateUrl: './webview-otp-request.component.html',
  styleUrls: ['./webview-otp-request.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class WebviewOtpRequestComponent implements OnInit {

  public webViewOtpSubscription: Subscription; loginSubscription: Subscription; 
         webviewVerifyOtpSubscription: Subscription;
  public webviewOtpResponse: Observable<Message>; loginResponse: Observable<Message>;
         webviewVerifyOtpResponse: Observable<Message>;
  public webviewForm: FormGroup;
  public mobileNumber: string;

  constructor(public formBuilder: FormBuilder, public aaWebSocket: AaWebsocketService, 
              public router: Router) {
    this.webviewForm = formBuilder.group({
      'otp': [null, Validators.required]
    });
  }

  ngOnInit() {
    this.mobileNumber = sessionStorage.getItem('userIdOrMobileNo');
    this.onLogin();
  }

  onLogin(){
    sessionStorage.removeItem("sid");
    this.loginResponse = this.aaWebSocket.on<Message>(MT.RES.LOGIN);
    const loginRequestPayload: LoginRequest = {
      username: sessionStorage.getItem("username"),
      password: sessionStorage.getItem("password")
    };

    if (loginRequestPayload.password == "" || loginRequestPayload.username == "" 
            || loginRequestPayload.password == null || loginRequestPayload.username == null) {
        return ;
    }

    const msgHeader = HeaderBuilder.build(null, MT.REQ.LOGIN);
    const loginRequestMessage: Message = {
      header: msgHeader,
      payload: loginRequestPayload
    };

    console.log(loginRequestMessage);
    this.aaWebSocket.send(loginRequestMessage);
    
    this.loginSubscription = this.loginResponse.subscribe(value => {
      console.log(value);
      sessionStorage.setItem("sid", value.header.sid);
      this.loginSubscription.unsubscribe();
       
      var reponseStatus = value.payload["status"] ;
      
      if (reponseStatus == RESPONSE_STATUS.SUCCESS) {
        this.webviewOtpRequest();
      } else if (reponseStatus == RESPONSE_STATUS.VERIFY) {
        this.webviewOtpRequest();
      } else if (reponseStatus == RESPONSE_STATUS.RETRY){
        this.onLogin();
      } else if (reponseStatus == RESPONSE_STATUS.FAILURE || reponseStatus == RESPONSE_STATUS.LOCKED) {
        alert(value.payload["message"])
      } else {
        alert("Something went wrong.")
      }
    });
  }

  webviewOtpRequest() {
    if(sessionStorage.getItem("sid") != null ) {
      this.webviewOtpResponse = this.aaWebSocket.on<Message>(MT.RES.WEBVIEWOTP);
      const webviewEncryptRequest: WebviewEncryptRequest = {
        encryptedRequest: sessionStorage.getItem("encryptedRequest"),
        requestDate: sessionStorage.getItem("reqdate"),
        encryptedFiuId: sessionStorage.getItem("encryptedFiuId")
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.WEBVIEWOTP);
  
      const WebviewOtpRequestMessage: Message = {
        header: msgHeader,
        payload: webviewEncryptRequest
      };
    
      console.log(WebviewOtpRequestMessage);
      this.aaWebSocket.send(WebviewOtpRequestMessage);
      this.webViewOtpSubscription = this.webviewOtpResponse.subscribe(value => {
        console.log(value);
        this.webViewOtpSubscription.unsubscribe();
        var reponseStatus = value.payload["status"] ;
        console.log("reponseStatus: ", reponseStatus);
        if (reponseStatus == RESPONSE_STATUS.SEND){
          alert(value.payload["message"])
        } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
          alert(value.payload["message"])
        } else if(reponseStatus == RESPONSE_STATUS.REJECT) {
          alert(value.payload["message"])
        } else{
          alert("Something went wrong.")
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  onMobileVerify() {
    if(sessionStorage.getItem("sid") != null ) {
      this.webviewVerifyOtpResponse = this.aaWebSocket.on<Message>(MT.RES.WEBVIEWVERIFYOTP);
      const webviewVerifyOtpPayload: MobileVerificationVerifyRequest = {
        mobileNum: sessionStorage.getItem('userIdOrMobileNo'),
        otp: this.webviewForm.get('otp').value
      };
      
      if (webviewVerifyOtpPayload.mobileNum == "" || webviewVerifyOtpPayload.mobileNum == null
          || webviewVerifyOtpPayload.otp == "" || webviewVerifyOtpPayload.otp == null) {
          return ;
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.WEBVIEWVERIFYOTP);

      const webviewVerifyOtpMessage: Message = {
        header: msgHeader,
        payload: webviewVerifyOtpPayload
      };

      console.log(webviewVerifyOtpMessage);
      this.aaWebSocket.send(webviewVerifyOtpMessage);
      
      this.webviewVerifyOtpSubscription = this.webviewVerifyOtpResponse.subscribe(value => {
        console.log(value);
        sessionStorage.setItem("sid", value.header.sid);
        this.webviewVerifyOtpSubscription.unsubscribe();
        
        var reponseStatus = value.payload["status"] ;
        if (reponseStatus == RESPONSE_STATUS.ACCEPT) {
          this.webviewForm.reset();
          alert(value.payload["message"])
          //this.router.navigate(['onboarding/login']);
          this.router.navigate(['onboarding/linked-account-message']);
        } else if (reponseStatus == RESPONSE_STATUS.REJECT|| reponseStatus == RESPONSE_STATUS.RETRY) {
          this.webviewForm.reset();
          alert(value.payload["message"])
        } else {
          alert("Something went wrong.")
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  submitForm() {
    this.markFormTouched(this.webviewForm);
    if (this.webviewForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.webviewForm.getRawValue;
      this.onMobileVerify();
    } 
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { 
        control.markAsTouched(); this.markFormTouched(control);
      } else { 
        control.markAsTouched(); 
      };
    });
  };
}
