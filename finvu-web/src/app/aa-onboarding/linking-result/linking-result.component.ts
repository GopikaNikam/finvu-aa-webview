import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-linking-result',
  templateUrl: './linking-result.component.html',
  styleUrls: ['./linking-result.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})

export class LinkingResultComponent implements OnInit {

  public fipName: string; username: string;
  public accounts = []; linkedAccounts = [];

  constructor(private router: Router) { }

  ngOnInit() {
    this.fipName = sessionStorage.getItem("fipName");
    this.username = sessionStorage.getItem("username");
    this.accounts = JSON.parse(sessionStorage.getItem("linkedAccounts"));
  }

  onOkClick() {
    if (sessionStorage.getItem('linkFlag')) {
      this.router.navigate(['onboarding/decrypt-consent-request']);
    } else {
      if(sessionStorage.getItem("fiuConsentRequestFlag")){
        this.router.navigate(['onboarding/fiu-consent-request']);
      } else {
        this.router.navigate(['onboarding/notifications']);
      } 
    }
  }
  
  onLinkAccountClick() {
    this.router.navigate(['onboarding/select-bank']);
  }
}
