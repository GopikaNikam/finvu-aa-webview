import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkingResultComponent } from './linking-result.component';

describe('LinkingResultComponent', () => {
  let component: LinkingResultComponent;
  let fixture: ComponentFixture<LinkingResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkingResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkingResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
