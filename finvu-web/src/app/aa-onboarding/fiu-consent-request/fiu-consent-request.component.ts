import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Message } from 'src/app/services/message';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { Router } from '@angular/router';
import { MT } from 'src/app/services/message-types';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { UserOfflineMessage } from 'src/app/services/payloads/user-offine-message';
import { UserLinkedAccountsRequest } from 'src/app/services/payloads/user-linked-accounts';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ConsentRequestDetails } from 'src/app/services/payloads/consent-request-details';
import { ConsentService } from 'src/app/services/consent-service';
import { UserOfflineMessageService } from 'src/app/services/user-offline-message-service';
import { DATA_LIFE_UNIT, ResponseStatus, RESPONSE_STATUS } from 'src/app/services/util';
import { EntityInfoRequest, EntityEnum } from 'src/app/services/payloads/entity-info-request';

@Component({
  selector: 'app-fiu-consent-request',
  templateUrl: './fiu-consent-request.component.html',
  styleUrls: ['./fiu-consent-request.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class FiuConsentRequestComponent implements OnInit {

  public userOfflineMessageResponse: Observable<Message>; userLinkedAccountResponse: Observable<Message>;
  consentRequestDetailsResponse: Observable<Message>; entityInfoResponse: Observable<Message>;
  public userOfflineMessageSubscription: Subscription; userLinkedAccountSubscription: Subscription;
  consentRequestDetailsSubscription: Subscription; entityInfoSubscription: Subscription;
  public consentRequestForm: FormGroup;
  public responseStatus: String; responseMessage: String; consentHandleId: String;
  handleStatus: String; fetchType: String;
  public userOfflineMesaage: any; consentRequestDetail: any;
  public linkedAccounts = []; accounts = []; checkBoxList = []; accountTypes = [];
  consentTypes = []; accountsWithIcons = []; accountInfo = []; 
  linkedAccountsCopy = [];
  public dateRanges: any; fromDate: any; toDate: any; messageText: any; messageTimestamp: any;
  fiTypes: any; frequency: any; dataLife: any; messageOriginatorName: any; purposeText: any; purpose: any;
  unit: any; value: any; icon: any; entityResponse: any; fipId: any; expireTime: any; consentMode: any
  dataLifeUnit: any; dataLifeValue: any; 
  public onetime: boolean = true; periodic: boolean = true; showDiv = false;
  fiuConsentRequestFlag: any = false; showAccounts = false;


  constructor(public aaWebSocket: AaWebsocketService, public router: Router,
    public formBuilder: FormBuilder, public consentService: ConsentService,
    public userOfflineMessageService: UserOfflineMessageService) { }

  ngOnInit() {
    this.consentRequestForm = this.formBuilder.group({
      selectAccount: ['', Validators.compose([Validators.required])]
    });

    this.userOfflineMessageResponse = this.aaWebSocket.on<Message>(MT.RES.USEROFFLINEMESSAGE);
    this.userLinkedAccountResponse = this.aaWebSocket.on<Message>(MT.RES.USERLINKEDACCOUNT);
    this.consentRequestDetailsResponse = this.aaWebSocket.on<Message>(MT.RES.CONSENTREQUESTDETAILS);
    this.entityInfoResponse = this.aaWebSocket.on<Message>(MT.RES.ENTITYINFO);

    if (sessionStorage.getItem("sid") != null) {
      this.userOffineMessages();
    } else {
      this.router.navigate(['onboarding/webview-login']);
      console.log("Session id not generated")
    }
  }

  getIcons() {
    if (sessionStorage.getItem("sid") != null) {      
      let loop = (linkAccount) => {
        const entityInfoRequestPayload: EntityInfoRequest = {
          entityId: linkAccount.fipId,
          entityType: EntityEnum.FIP
        }
        const msgHeader = HeaderBuilder.build(null, MT.REQ.ENTITYINFO);
        const entityInfoRequestMessage: Message = {
          header: msgHeader,
          payload: entityInfoRequestPayload
        };
      
        this.aaWebSocket.send(entityInfoRequestMessage);
        this.entityInfoSubscription = this.entityInfoResponse.subscribe(value => {
          this.entityInfoSubscription.unsubscribe();
            var icon = value.payload['entityIconUri'] == null ? 'assets/images/bank_large_light.png' : value.payload['entityIconUri'].replace('www.', '')  
            this.accountsWithIcons.push({
              icon: {
                icon: icon
              },
              accType: linkAccount.accType,
              maskedAccNumber: linkAccount.maskedAccNumber,
              FIType: linkAccount.FIType,
              fipName: linkAccount.fipName,
              fipId: linkAccount.fipId,
              accRefNumber: linkAccount.accRefNumber,
              linkRefNumber: linkAccount.linkRefNumber
            })
            if(this.accounts.length){
              loop(this.accounts.shift())
            }
        })
      }
      loop(this.accounts.shift());
   } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  userOffineMessages() {
    const userOfflineMessagePayload: UserOfflineMessage = {
      userId: sessionStorage.getItem("username"),
      messageId: sessionStorage.getItem("messageId")
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.USEROFFLINEMESSAGE);

    const userOfflineMessage: Message = {
      header: msgHeader,
      payload: userOfflineMessagePayload
    };

    console.log("User Offline Message Request :- " + JSON.stringify(userOfflineMessage));
    this.aaWebSocket.send(userOfflineMessage);
    this.userOfflineMessageSubscription = this.userOfflineMessageResponse.subscribe(value => {
      console.log("User Offline Message Response :- " + JSON.stringify(value));
      this.userOfflineMessageSubscription.unsubscribe();
      this.responseStatus = value.payload["status"];
      this.responseMessage = value.payload["message"];

      if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
        this.userOfflineMesaage = value.payload["offlineMessage"]
        this.messageText = this.userOfflineMesaage.messageOriginatorName

        const entityInfoRequestPayload: EntityInfoRequest = {
          entityId: this.userOfflineMesaage.messageOriginator,
          entityType: EntityEnum.FIU
        }
        const msgHeader = HeaderBuilder.build(null, MT.REQ.ENTITYINFO);
        const entityInfoRequestMessage: Message = {
          header: msgHeader,
          payload: entityInfoRequestPayload
        };
      
        this.aaWebSocket.send(entityInfoRequestMessage);
        this.entityInfoSubscription = this.entityInfoResponse.subscribe(value => {
          this.entityInfoSubscription.unsubscribe();
          this.icon = value.payload['entityIconUri'] == null ? 'assets/images/bank_large_light.png' : value.payload['entityIconUri'].replace('www.', '')  
        })

        this.messageTimestamp = this.userOfflineMesaage.messageTimestamp
        this.purposeText = this.userOfflineMesaage.messageText
        this.purposeText = this.purposeText.substr(this.purposeText.indexOf("for")).substring(4);
        sessionStorage.setItem("fiuName", this.userOfflineMesaage.messageOriginatorName);
        console.log(sessionStorage.getItem("fiuName"));
        sessionStorage.setItem("requestConsentId", this.userOfflineMesaage.requestConsentId);
        this.consentRequestDetails();
        this.userLinkedAccounts();
      } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
        alert(this.responseMessage)
      } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
        alert(value.payload["message"])
      } else {
        alert(this.responseMessage)
      }
    });
  }

  userLinkedAccounts() {
    if (sessionStorage.getItem("sid") != null) {
      const userLinkedAccountsRequestPayload: UserLinkedAccountsRequest = {
        userId: sessionStorage.getItem("username")
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERLINKEDACCOUNT);

      const userLinkedAccountsRequestMessage: Message = {
        header: msgHeader,
        payload: userLinkedAccountsRequestPayload
      };

      console.log("User Linked Accounts Request :- " + JSON.stringify(userLinkedAccountsRequestMessage));
      this.aaWebSocket.send(userLinkedAccountsRequestMessage);

      this.userLinkedAccountSubscription = this.userLinkedAccountResponse.subscribe(value => {

        console.log("User Linked Accounts Response :- " + JSON.stringify(value));
        this.userLinkedAccountSubscription.unsubscribe();

        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {

          this.linkedAccounts = value.payload["LinkedAccounts"];
          console.log("Linked Accounts: ", JSON.stringify(this.linkedAccounts));

          this.linkedAccounts.map(item => {
            item.fipId == "DEFAULT" ? item.accType = item.FIType : item.accType;
            return {
              accType: item.accType
            }
          }).forEach(item => this.accountTypes.push(item.accType));

          this.linkedAccounts.map(item => {
            item.accType == "DEFAULT" ? item.accType = item.FIType : item.accType;
            return {
              accType: item.accType
            }
          }).forEach(item => this.accountTypes.push(item.accType));

          this.accountTypes = this.removewithfilter(this.accountTypes).join(', ');
          console.log("Account Types: ", this.accountTypes);

          this.linkedAccounts.map(item => {
            return {
              fipId: item.fipId,
              maskedAccNumber: item.maskedAccNumber,
              accRefNumber: item.accRefNumber,
              linkRefNumber: item.linkRefNumber,
              FIType: item.FIType,
              accType: item.accType,
              fipName: item.fipName
            }
          }).forEach(item => this.accounts.push(item));
          console.log("Accounts: "+ JSON.stringify(this.accounts));
          this.getIcons();
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          alert(this.responseMessage)
        } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          console.log(value.payload["message"])
          this.showAccounts = true
        } else {
          alert(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  consentRequestDetails() {
    if (sessionStorage.getItem("sid") != null) {
      const consentRequestDetailsPayload: ConsentRequestDetails = {
        consentHandleId: sessionStorage.getItem("requestConsentId"),
        userId: sessionStorage.getItem("username")
      };

      const msgHeader = HeaderBuilder.build(null, MT.REQ.CONSENTREQUESTDETAILS);

      const consentRequestDetailsMessage: Message = {
        header: msgHeader,
        payload: consentRequestDetailsPayload
      };

      console.log("Consent Request Details Request:- " + JSON.stringify(consentRequestDetailsMessage));
      this.aaWebSocket.send(consentRequestDetailsMessage);
      this.consentRequestDetailsSubscription = this.consentRequestDetailsResponse.subscribe(value => {
        console.log("Consent Request Details Response:- " + JSON.stringify(value.payload));
        this.consentRequestDetail = JSON.stringify(value.payload)
        this.consentRequestDetailsSubscription.unsubscribe();
        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == ResponseStatus.Success) {
          this.fetchType = value.payload['fetchType']
          this.getFetchType();
          this.consentTypes = value.payload['consentTypes'].join(', ');
          this.fiTypes = value.payload['fiTypes'].join(', ');
          this.dateRanges = value.payload['DataDateTimeRange']
          this.fromDate = this.dateRanges.from
          this.toDate = this.dateRanges.to
          this.frequency = value.payload['Frequency']
          this.unit = this.frequency['unit']
          this.value = this.frequency['value']
          this.expireTime = value.payload['expireTime']
          this.consentMode = value.payload['mode']
          this.dataLife = value.payload['DataLife']
          var result = value.payload['Purpose']
          this.purpose = result['text']
          this.getDataLife(this.dataLife.unit, this.dataLife.value)
          sessionStorage.setItem("ConsentDetails", this.consentRequestDetail);
        } else if (this.responseStatus == ResponseStatus.Failure) {
          alert(this.responseMessage)
        } else if (this.responseStatus == ResponseStatus.Record_not_Found) {
          alert(value.payload["message"])
        } else {
          alert(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding/webview-login']);
      console.log("Session id not generated")
    }
  }

  getDataLife(unit, value) {
    if(unit == 'INF') {
      this.dataLifeUnit = DATA_LIFE_UNIT.INF
    } else if(unit == 'MONTH') {
      this.dataLifeUnit = DATA_LIFE_UNIT.MONTH
    } else if(unit == 'YEAR') {
      this.dataLifeUnit = DATA_LIFE_UNIT.YEAR
    } else if(unit == 'DAY') {
      this.dataLifeUnit = DATA_LIFE_UNIT.DAY
    }

    if(value > 0) {
      this.dataLifeValue = value
    }
  }

  getFetchType() {
    if (this.fetchType == "ONETIME") {
      this.onetime = true;
      this.periodic = false;
    } else {
      this.periodic = true;
      this.onetime = false;
    }
  }

  removewithfilter(arr) {
    let outputArray = arr.filter(function (v, i, self) {
      // It returns the index of the first 
      // instance of each value 
      return i == self.indexOf(v);
    });
    console.log(outputArray);
    return outputArray;
  }

  onCheckboxChange(account: Account, event: any) {
    if (event.target.checked) {
      this.checkBoxList.push(account);
      sessionStorage.setItem("accounts", JSON.stringify(this.checkBoxList))
    }
    else {
      for (var i = 0; i < this.checkBoxList.length; i++) {
        if (this.checkBoxList[i] == account) {
          this.checkBoxList.splice(i, 1);
        }
      }
    }
  }

  isSelected(account: Account) {
    return this.checkBoxList.indexOf(account) >= 0;
  }

  aggregateAccountByFipId() {
    this.checkBoxList.forEach(linkAccount => {
        let accInfo = this.accountInfo.find(obj => obj.FIP.Id == linkAccount.fipId);
        if(accInfo){
            accInfo.Accounts.push({
              linkRefNumber: linkAccount.linkRefNumber,
              accType: linkAccount.accType,
              accRefNumber: linkAccount.accRefNumber,
              maskedAccNumber: linkAccount.maskedAccNumber,
              FIType: linkAccount.FIType,
              fipId: linkAccount.fipId,
              fipName: linkAccount.fipName
            })
        }else{
            this.accountInfo.push({
                FIP: {
                    id: linkAccount.fipId
                },
                Accounts: [{
                  linkRefNumber: linkAccount.linkRefNumber,
                  accType: linkAccount.accType,
                  accRefNumber: linkAccount.accRefNumber,
                  maskedAccNumber: linkAccount.maskedAccNumber,
                  FIType: linkAccount.FIType,
                  fipId: linkAccount.fipId,
                  fipName: linkAccount.fipName
                }]
            })
        }
    })
    console.log("Accounts:- " +JSON.stringify(this.accountInfo));
  }

  onNextClick() {
    if (sessionStorage.getItem("sid") != null) {
      this.aggregateAccountByFipId();
      this.handleStatus = this.HANDLE_STATUS.ACCEPT
      sessionStorage.setItem("consentStatus", this.handleStatus.toString())
      this.consentService.consentRequest(this.handleStatus, JSON.parse(sessionStorage.getItem("ConsentDetails")), this.accountInfo);
      this.router.navigate(['onboarding/consent-confirmation']);    
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  onCancelClick() {
    if (sessionStorage.getItem("sid") != null) {
      this.router.navigate(['onboarding/decline-consent-request']);   
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  public selectValidator(control: FormControl) {
    console.log(JSON.stringify(this.checkBoxList))
    if (typeof this.checkBoxList === 'undefined' || this.checkBoxList === null || this.checkBoxList.length <= 0)
      return { 'atleast': true };
  }

  submitForm() {
    this.markFormTouched(this.consentRequestForm);
    if (this.consentRequestForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.consentRequestForm.getRawValue;
      if (this.checkBoxList.length <= 0) {
        alert("Select atleast one account");
         return 
       } 
      this.onNextClick();
    } else {
      console.log("Something went wrong");
    }
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) {
        control.markAsTouched(); this.markFormTouched(control);
      } else {
        control.markAsTouched();
      };
    });
  };

  onLinkAccountClick() {
    this.fiuConsentRequestFlag = true;
    sessionStorage.setItem("fiuConsentRequestFlag", this.fiuConsentRequestFlag);
    this.router.navigate(['onboarding/select-bank']);
  }

  private HANDLE_STATUS = {
    ACCEPT: "ACCEPT",
    REJECT: "DENY"
  };
}