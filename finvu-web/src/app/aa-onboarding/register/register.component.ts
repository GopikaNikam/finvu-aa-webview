import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Message } from 'src/app/services/message';
import { MT } from 'src/app/services/message-types';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { WebViewEncryptedRequest, UserRegistrationRequest, WebViewRegistration } from 'src/app/services/payloads/register';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { ConfigService } from 'src/app/services/config/config-service';
import { RESPONSE_STATUS } from 'src/app/services/util';
import { StayAliveRequestService } from 'src/app/services/stay-alive-service';
import { ConfirmedValidator } from 'src/app/services/password-validation';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  public subscriptionRegister: Subscription;
  public registerResponse: Observable<Message>;
  public ecreq: any; fiuId: any; reqdate: any; aaId: any; 
  public linkFlag: any = true; hide = true; confirmHide = true;
  public userPostfixName: string; mobileNumber: string

  constructor(private formBuilder: FormBuilder, private router: Router, 
              private aaWebSocket: AaWebsocketService, private configService: ConfigService,
              private stayAliveRequestService: StayAliveRequestService) {
    this.userPostfixName = this.configService.userPostfixName
  }

  ngOnInit() {

    this.stayAliveRequestService.callTimeoutMethod();
    this.mobileNumber = sessionStorage.getItem('userIdOrMobileNo');

    this.registerForm = this.formBuilder.group({
      aaId: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._-]{6,30}$')])],
      mobile: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{4,4}$')])],
      reEnterPassword: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{4,4}$')])],
      termCondition: ['', [Validators.required, Validators.pattern('true')]]
    }, {
      validator: ConfirmedValidator('password', 'reEnterPassword')
    });
  }

  onRegisterClick() {

    sessionStorage.removeItem("sid");
    this.registerResponse = this.aaWebSocket.on<Message>(MT.RES.WEBVIEWREGISTER);

    this.aaId = this.registerForm.get('aaId').value.includes('@finvu')
      ? this.registerForm.get('aaId').value
      : this.registerForm.get('aaId').value + this.userPostfixName

    const webViewEncryptedRequest: WebViewEncryptedRequest = {
      encryptedRequest: sessionStorage.getItem("encryptedRequest"),
      requestDate: sessionStorage.getItem("reqdate"),
      encryptedFiuId: sessionStorage.getItem("encryptedFiuId") 
    }

    const userRegistrationRequest: UserRegistrationRequest = {
      mobileno: sessionStorage.getItem('userIdOrMobileNo'),
      password: this.registerForm.get('password').value,
      repassword: this.registerForm.get('reEnterPassword').value,
      username: this.aaId,
    }

    const webViewRegistrationPayload: WebViewRegistration = {
      webViewEncryptedRequest: webViewEncryptedRequest,
      registrationRequest: userRegistrationRequest
    };

    sessionStorage.setItem("username", this.aaId.toString());
    sessionStorage.setItem("password", this.registerForm.get('password').value)
    sessionStorage.setItem("mobileNo", this.registerForm.get('mobile').value)

    const msgHeader = HeaderBuilder.build(null, MT.REQ.WEBVIEWREGISTER);

    const registerRequestMessage: Message = {
      header: msgHeader,
      payload: webViewRegistrationPayload
    };

    console.log("Register Request: "+ JSON.stringify(registerRequestMessage));
    this.aaWebSocket.send(registerRequestMessage);

    this.subscriptionRegister = this.registerResponse.subscribe(value => {
      console.log(value);
      console.log("Register SessionId", value.header.sid);
      this.subscriptionRegister.unsubscribe();
      var reponseStatus = value.payload["status"];

      if (reponseStatus == RESPONSE_STATUS.SUCCESS) {
        sessionStorage.setItem("linkFlag", this.linkFlag);
        this.router.navigate(['onboarding/webview-otp-request']);
      } else if (reponseStatus == RESPONSE_STATUS.FAILURE 
        || reponseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
        alert(value.payload["message"])
      } else {
        alert("Something went wrong.")
      }
    });
  }

  passwordMatchValidator(control: AbstractControl) {
    const password: string = control.get('password').value; 
    const confirmPassword: string = control.get('reEnterPassword').value;
    if (password !== confirmPassword) {
      // if they don't match, set an error in our confirmPassword form control
      control.get('reEnterPassword').setErrors({ NoPassswordMatch: true });
    }
  }
}
