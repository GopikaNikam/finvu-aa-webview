import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebviewRegisterComponent } from './webview-register.component';

describe('WebviewRegisterComponent', () => {
  let component: WebviewRegisterComponent;
  let fixture: ComponentFixture<WebviewRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebviewRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebviewRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
