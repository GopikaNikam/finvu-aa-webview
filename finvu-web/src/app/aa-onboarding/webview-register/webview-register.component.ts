import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { Message } from 'src/app/services/message';
import { Router } from '@angular/router';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { RegisterRequest } from 'src/app/services/payloads/register';
import { MT } from 'src/app/services/message-types';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { ConfigService } from 'src/app/services/config/config-service';
import { RESPONSE_STATUS } from 'src/app/services/util';
import { ConfirmedValidator } from 'src/app/services/password-validation';

@Component({
  selector: 'app-webview-register',
  templateUrl: './webview-register.component.html',
  styleUrls: ['./webview-register.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class WebviewRegisterComponent implements OnInit {

  public registerForm: FormGroup;
  public subscriptionRegister: Subscription;
  public registerResponse: Observable<Message>;
  public userPostfixName;
  public aaId: string; hide = true; confirmHide = true;

  constructor(private formBuilder: FormBuilder, private router: Router,
    private aaWebSocket: AaWebsocketService, private configService: ConfigService) { 
      this.userPostfixName = this.configService.userPostfixName
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      aaId: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._-]{6,30}$')])],
      mobile: ['', Validators.compose([Validators.required, Validators.pattern('[6-9]\\d{9}')])],
      termCondition: ['', Validators.compose([Validators.required, Validators.pattern('true')])],
      password: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{4,4}$')])],
      reEnterPassword: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{4,4}$')])],
    }, {
      validator: ConfirmedValidator('password', 'reEnterPassword')
    });
  }

  onRegisterClick() {
    sessionStorage.removeItem("sid");
    this.registerResponse = this.aaWebSocket.on<Message>(MT.RES.REGISTER);

    if (this.registerForm.get('aaId').value.includes('@finvu.in')) {
      alert("Please enter valid username with @finvu");
    } else {
      this.aaId = this.registerForm.get('aaId').value.includes('@finvu')
        ? this.registerForm.get('aaId').value.trim()
        : this.registerForm.get('aaId').value.trim() + this.userPostfixName;

      const registerRequestPayload: RegisterRequest = {
        mobileno: this.registerForm.get('mobile').value,
        username: this.aaId,
        password: this.registerForm.get('password').value,
        repassword: this.registerForm.get('reEnterPassword').value
      };

      sessionStorage.setItem("username", this.aaId.toString())
      sessionStorage.setItem("password", this.registerForm.get('password').value)
      sessionStorage.setItem("mobileNo", this.registerForm.get('mobile').value)

      if (registerRequestPayload.password == "" || registerRequestPayload.password == null
        || registerRequestPayload.username == "" || registerRequestPayload.username == null
        || registerRequestPayload.repassword == "" || registerRequestPayload.repassword == null
        || registerRequestPayload.mobileno == "" || registerRequestPayload.mobileno == null) {
        return;
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.REGISTER);
      const registerRequestMessage: Message = {
        header: msgHeader,
        payload: registerRequestPayload
      };

      console.log(registerRequestMessage);
      this.aaWebSocket.send(registerRequestMessage);

      this.subscriptionRegister = this.registerResponse.subscribe(value => {
        console.log(value);
        console.log("Register SessionId", value.header.sid);
        this.subscriptionRegister.unsubscribe();
        var reponseStatus = value.payload["status"];
        if (reponseStatus == RESPONSE_STATUS.SUCCESS){
          this.router.navigate(['onboarding/mobile-verification']);
        } else if (reponseStatus == RESPONSE_STATUS.FAILURE 
          || reponseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          alert(value.payload["message"])
        } else {
          alert("Something went wrong.")
        }
      });
    }
  }

  onLoginClick() {
    this.router.navigate(['onboarding/webview-login']);
  }
}
 
