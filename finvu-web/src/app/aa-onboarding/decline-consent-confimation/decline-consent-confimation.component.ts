import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription, timer } from 'rxjs';
import { PostMesaageService } from 'src/app/services/post-mesaage-service';
import { ACTIONS } from 'src/app/services/util';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-decline-consent-confimation',
  templateUrl: './decline-consent-confimation.component.html',
  styleUrls: ['./decline-consent-confimation.component.css',
  '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class DeclineConsentConfimationComponent implements OnInit {

  public subscriptionTimer: Subscription; 
  public counter$: Observable<number>; counterTime$: Observable<number>; count = 30;
  public fiuName: any; handleStatus: String;

  constructor(private router: Router, private postMesaageService: PostMesaageService) {}

  ngOnInit() {
    this.fiuName = sessionStorage.getItem("fiuName")
    this.handleStatus = sessionStorage.getItem("consentStatus");
    this.setTimer();
  }

  okClick(){
    this.cancelTimer() 
    this.postMesaageService.postMessage(ACTIONS.CONSENT, this.handleStatus);
    this.router.navigate(['onboarding/notifications']);
  }

  public cancelTimer(){
    this.subscriptionTimer.unsubscribe();
  }

  public setTimer() {
    this.count = 5;
    this.counterTime$ = timer(0, 1000).pipe(
      take(this.count),
      map(() => --this.count)
    );
    this.counter$ = timer(5000);
    this.subscriptionTimer = this.counter$.subscribe(() => {
      if(this.count == 0) {
        this.okClick();
      } 
    });
  }
}
