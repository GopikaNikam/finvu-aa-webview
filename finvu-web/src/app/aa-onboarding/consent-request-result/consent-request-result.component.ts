import { Component, OnInit } from '@angular/core';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { Message } from 'src/app/services/message';
import { LogoutRequest } from 'src/app/services/payloads/logout';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { MT } from 'src/app/services/message-types';
import { Observable, timer, Subscription } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { PostMesaageService } from 'src/app/services/post-mesaage-service';
import { ACTIONS } from 'src/app/services/util';

@Component({
  selector: 'app-consent-request-result',
  templateUrl: './consent-request-result.component.html',
  styleUrls: ['./consent-request-result.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class ConsentRequestResultComponent implements OnInit {

  public consentHandleId: String; custId: String; consentId: String; sessionId:String; 
  fiuName: string; handleStatus: String;
  public dateTimeRangeFrom: Date; dateTimeRangeTo: Date; encryptResponse: any; 
  public counter$: Observable<number>; counterTime$: Observable<number>; count = 30;
  public subscriptionTimer: Subscription; 
   
  constructor(private aaWebSocket: AaWebsocketService, 
              private postMesaageService: PostMesaageService) {}

  ngOnInit() {
    this.custId = sessionStorage.getItem("username");
    this.fiuName = sessionStorage.getItem("fiuName");
    this.handleStatus = sessionStorage.getItem("consentStatus");
    this.setTimer();
  }

  onOkClick(){
    this.cancelTimer();
    this.userLogout();
    this.postMesaageService.postMessage(ACTIONS.CONSENT, this.handleStatus);
    this.encryptResponse = JSON.parse(sessionStorage.getItem("EncryptResponse"));
    if(this.encryptResponse['redirectUrl'].includes('?')) {
      window.location.href = this.encryptResponse['redirectUrl'] + "&ecres="+this.encryptResponse["encryptedResponse"]+"&"+"resdate="+ this.encryptResponse['responseDate']+"&"+"fi="+ this.encryptResponse['encryptedFiId'];
    } else {
      window.location.href = this.encryptResponse['redirectUrl'] + "?ecres="+this.encryptResponse["encryptedResponse"]+"&"+"resdate="+ this.encryptResponse['responseDate']+"&"+"fi="+ this.encryptResponse['encryptedFiId'];
    }
  }

  userLogout(){
    console.log("user " + this.custId + "logout") ;
    const logoutRequestPayload: LogoutRequest = {
      username: this.custId,
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.LOGOUT);

    const logoutRequestMessage: Message = {
      header: msgHeader,
      payload: logoutRequestPayload
    };
    console.log("Logout Request : " + JSON.stringify(logoutRequestMessage));
    this.aaWebSocket.send(logoutRequestMessage);
  }

  public cancelTimer(){
    this.subscriptionTimer.unsubscribe();
  }

  public setTimer() {
    this.count = 5;
    this.counterTime$ = timer(0, 1000).pipe(
      take(this.count),
      map(() => --this.count)
    );
    this.counter$ = timer(5000);
    this.subscriptionTimer = this.counter$.subscribe(() => {
      if(this.count == 0) {
        this.onOkClick();
      } 
    });
  }
}