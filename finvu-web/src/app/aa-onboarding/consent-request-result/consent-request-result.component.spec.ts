import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsentRequestResultComponent } from './consent-request-result.component';

describe('ConsentRequestResultComponent', () => {
  let component: ConsentRequestResultComponent;
  let fixture: ComponentFixture<ConsentRequestResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsentRequestResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentRequestResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
