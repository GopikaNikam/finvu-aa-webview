import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Message } from 'src/app/services/message';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config-service';
import { MT } from 'src/app/services/message-types';
import { WebviewEncryptRequest } from 'src/app/services/payloads/webview-encrypt-request';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { RESPONSE_STATUS } from 'src/app/services/util';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class OnboardingComponent implements OnInit {

  public webviewResponse: Observable<Message>;
  public webviewSubscription: Subscription;
  public responseStatus: String; responseMessage: String; userIdOrMobileNo: string; username: string; 
  public ecreq: any; reqdate: any; fiuId: any; callBackUrl: any; keepAliveTimeOut: any; keepAliveURL: any;
  keepAlive: any = false;

  constructor(private aaWebSocket: AaWebsocketService, private activatedRoute: ActivatedRoute,
              private router: Router) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.ecreq = params['ecreq'];
      this.reqdate = params['reqdate'];
      this.fiuId = params['fi'];
    });
    sessionStorage.setItem("encryptedRequest", this.ecreq);
    sessionStorage.setItem("encryptedFiuId", this.fiuId);
    sessionStorage.setItem("reqdate", this.reqdate);
    this.username = sessionStorage.getItem('username');
    this.webviewEncryptRequest();
  }

  webviewEncryptRequest() {

    sessionStorage.removeItem("sid");
    this.webviewResponse = this.aaWebSocket.on<Message>(MT.RES.WEBVIEWENCRYPTEDREQUEST);

    const webviewEncryptRequest: WebviewEncryptRequest = {
      encryptedRequest: sessionStorage.getItem("encryptedRequest"),
      requestDate: sessionStorage.getItem("reqdate"),
      encryptedFiuId: sessionStorage.getItem("encryptedFiuId")
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.WEBVIEWENCRYPTEDREQUEST);

    const webviewEncryptRequestMessage: Message = {
      header: msgHeader,
      payload: webviewEncryptRequest
    };

    console.log("Webview Encrypt Request: " + JSON.stringify(webviewEncryptRequestMessage));
    if(webviewEncryptRequest.encryptedRequest == null
      || webviewEncryptRequest.encryptedRequest == ""
      || webviewEncryptRequest.encryptedRequest == "undefined"
      || webviewEncryptRequest.requestDate == null
      || webviewEncryptRequest.requestDate == ""
      || webviewEncryptRequest.requestDate == "undefined"
      || webviewEncryptRequest.encryptedFiuId == null
      || webviewEncryptRequest.encryptedFiuId == ""
      || webviewEncryptRequest.encryptedFiuId == "undefined") {
      console.log("Required values missing")
      this.router.navigate(['onboarding/webview-login']);
    } else {
        this.aaWebSocket.send(webviewEncryptRequestMessage);
        this.webviewSubscription = this.webviewResponse.subscribe(value => {
          console.log("Webview Encrypt Response: " + JSON.stringify(value));
          this.webviewSubscription.unsubscribe();
    
          this.responseStatus = value.payload["status"];
          this.responseMessage = value.payload["message"];
    
          if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
            this.userIdOrMobileNo = value.payload["userIdOrMobileNo"];
            if(value.payload['keepAliveTimeout'] > 0 
                && value.payload['keepAliveUrl'] != null
                && value.payload['keepAliveUrl'] != "" ) {
                this.keepAlive = true;
                sessionStorage.setItem("keepAlive", this.keepAlive);
                sessionStorage.setItem("keepAliveTimeOut", value.payload['keepAliveTimeout']);
                sessionStorage.setItem("keepAliveUrl", value.payload['keepAliveUrl']);
            }
            sessionStorage.setItem("userIdOrMobileNo", this.userIdOrMobileNo);
            if (this.userIdOrMobileNo.includes("@")) {
              this.router.navigate(['onboarding/login']);
            } else if (!this.userIdOrMobileNo.includes("@")) {
              sessionStorage.setItem("userIdOrMobileNo", this.userIdOrMobileNo);
              this.router.navigate(['onboarding/register']);
            }
          } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
              console.log(this.responseMessage);
              this.router.navigate(['onboarding/onboarding-error-page']);
          }
        });
      }
  }  
}
