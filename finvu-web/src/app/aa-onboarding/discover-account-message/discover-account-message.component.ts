import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-discover-account-message',
  templateUrl: './discover-account-message.component.html',
  styleUrls: ['./discover-account-message.component.css',
  '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class DiscoverAccountMessageComponent implements OnInit {

  public fipName: string;
  
  constructor(public router: Router) { }

  ngOnInit() {
    this.fipName = sessionStorage.getItem("fipName");
  }

  onOkClick() {
    if(sessionStorage.getItem("fiuConsentRequestFlag")){
      this.router.navigate(['onboarding/fiu-consent-request']);
    } else if(sessionStorage.getItem("decryptConsentRequestFlag")) {
      this.router.navigate(['onboarding/decrypt-consent-request']);
    } else {
      this.router.navigate(['onboarding/select-bank']);
    } 
  }
}
