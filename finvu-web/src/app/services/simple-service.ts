import { Injectable, Inject } from '@angular/core';
import { WINDOW } from 'src/app/services/window.providers';

@Injectable()
export class SimpleService {

    constructor(@Inject(WINDOW) private window: Window) {
    }

    getHostname() : string {
        return this.window.location.hostname;
    }
}