import { TestBed } from '@angular/core/testing';

import { AaWebsocketService } from './aa-websocket.service';

describe('AaWebsocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AaWebsocketService = TestBed.get(AaWebsocketService);
    expect(service).toBeTruthy();
  });
});
