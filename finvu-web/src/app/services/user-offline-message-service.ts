import { Injectable } from '@angular/core';
import { HeaderBuilder } from './header-builder';
import { MT } from './message-types';
import { Message } from './message';
import { AaWebsocketService } from './websocket/aa-websocket.service';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UserOfflineMessage } from './payloads/user-offine-message';
import { ResponseStatus } from 'src/app/services/util';

@Injectable({
    providedIn: 'root'
})

export class UserOfflineMessageService {
   
  public updateUserOfflineMessageAckResponse: Observable<Message>;
  public updateUserOfflineMessageAckSubscription: Subscription;
  public responseStatus: String; 
  public responseMessage: String; 
  
  constructor(public aaWebSocket: AaWebsocketService, public router: Router) {}

  updateUserOffineMessageAck() {

    this.updateUserOfflineMessageAckResponse = this.aaWebSocket.on<Message>(MT.RES.UPDATEUSEROFFLINEMESSAGEACK);

    const userOfflineMessagePayload: UserOfflineMessage = {
      userId: sessionStorage.getItem("username"),
      messageId: sessionStorage.getItem("messageId")
    };
    
    const msgHeader = HeaderBuilder.build(null, MT.REQ.UPDATEUSEROFFLINEMESSAGEACK);
    
    const userOfflineMessage: Message = {
      header: msgHeader,
      payload: userOfflineMessagePayload
    };
    
    console.log("Update User Offline Message Ack Request :- " + JSON.stringify(userOfflineMessage));
    this.aaWebSocket.send(userOfflineMessage);
    this.updateUserOfflineMessageAckSubscription = this.updateUserOfflineMessageAckResponse.subscribe(value => {
    this.updateUserOfflineMessageAckSubscription.unsubscribe();
    this.responseStatus = value.payload["status"] ;
    this.responseMessage = value.payload["message"] ;
          
    if (this.responseStatus == ResponseStatus.Success) {
      console.log("Update User Offline Message Ack Response :- " + JSON.stringify(value));
    } else if (this.responseStatus == ResponseStatus.Failure) {
      alert(this.responseMessage)
      console.log("Update User Offline Message Ack Response :- " + JSON.stringify(value));
    } else if (this.responseStatus == ResponseStatus.Record_not_Found) {
      alert(value.payload["message"])
      console.log("Update User Offline Message Ack Response :- " + JSON.stringify(value));
    } else {
        alert(this.responseMessage)
    }
  });
 }
}
   
    


