export interface RegisterRequest {
    mobileno: String
    username: String
    password: String
    repassword: String
}

export interface WebViewEncryptedRequest {
    encryptedRequest: any
    requestDate: any
    encryptedFiuId: any
}

export interface UserRegistrationRequest {
    username: string
    mobileno: string
    password: string
    repassword: string
}

export interface WebViewRegistration {
    webViewEncryptedRequest: WebViewEncryptedRequest
    registrationRequest: UserRegistrationRequest
}
