export interface DiscoveryRequest {
    ver: String
    timestamp: Date
    txnid: String
    Customer: Customer
    FIPDetails: FIPDetails 
    FITypes: Array<string>
}

export interface Customer {
    id: String
    Identifiers: Array<Identifier>
}

export interface Identifier {
    category: String
    type: String
    value: String
}

export interface FIPDetails {
    fipId: String
    fipName: String
}
