export interface MobileVerificationVerifyRequest {
    mobileNum: String
    otp: String
}