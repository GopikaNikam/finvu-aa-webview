export interface VerifyForgotPasswordRequest {
    userId: String
    mobileNum: String
    otp: String
    newPassword: String
    confirmPassword: String
}
