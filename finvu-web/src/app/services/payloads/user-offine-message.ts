export interface UserOfflineMessages {
    userId: String
}

export interface UserOfflineMessage {
    userId: String
    messageId: String
}