import { MsgHeader } from "./msg-header";

export interface Message {
    header: MsgHeader
    payload: Object
}