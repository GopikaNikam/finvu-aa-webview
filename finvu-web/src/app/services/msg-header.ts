export interface MsgHeader {
    mid: string
    ts: Date
    sid: string
    dup: boolean
    type: string
}